<?php 
if( $argv[1]== 'new' && $argv[2]== 'flavor'){
    $flavorName = $argv[3];
    echo "New " . $flavorName . " created\n";

    $flavorTemplate = "    

        class ". $flavorName . "Flavor extends Flavor {

            public function scoop( \WP_Post \$post = null ){

                \$json = [
                        '@context'  =>  'http://schema.org',
                ];

                return \$json;
            }

    }";

    $flavorFile = fopen(dirname(__FILE__) . "/IceScheme/flavors/". $flavorName .".php","wb");
    fwrite($flavorFile, $flavorTemplate );
    fclose($flavorFile);
    echo "New " . $flavorName . " created\n";
}