**IceScheme Schema Generator**

IceScheme is a dynamic schema generator based on schema models called Flavors. Each flavor is a pre-defined schema type that is built and outputted as JSON+LD in the `wp_footer`. 

**How it works**

IceScheme works by allowing developers to create pre-defined schema models based on the structures on Schema.org. These models are populated by dynamic data pulled by the current post, and outputted as JSON+LD. Each model is called a Flavor, and represents a singular property or object. These Flavors can be modularly built, one calling many other flavors, all dynamically populating. 


---

## Create a Flavor

You'll start by creating a flavor model based on a schema property from http://schema.org. Note: This will soon become a nodejs CLI tool instead of php.

1. Navigate to your IceScheme folder in your bash console.
2. run `php icescheme.php new flavor BlogPost`.
3. This will create a `/flavors/BlogPost.php` containing the `BlogPostFlavor` class.
4. Inside this class, the `scoop()` method, you can use this method to return an array with the structure of the schema object.
5. Then, inside your template, simply use `IceScheme('BlogPost')->build();` and your schema will be built and outputted to the footer as LD+JSON.

## Examples

1. `php icescheme.php new flavor MyPerson`

2. `/flavors/MyPerson.php` will now contain:

```
    class MyPersonFlavor extends Flavor {

        public function scoop( \WP_Post $post = null ){

            $json = [
                    '@context'  =>  'http://schema.org',
            ];

            return $json;
        }
    }

```

In the `$json` array, we simply complete the structure as per http://schema.org/Person.


```
            $json = [
                    '@context'  =>  'http://schema.org',
                    '@type'     =>  'Person',
                    'name'      =>  'Hank Hill',
                    'url'       =>  get_the_permalink($post->id),
                    'email'     =>  'hankhill@stricklandpropane.com'
            ];
``` 

Finally, on our template file, just include:

`IceScheme('MyPerson', $post)->build();`


This will output the following in the footer:

```
<script type="application/ld+json">
    {
        "@context":"http://schema.org",
        "@type":"Person",
        "name":"Hank Hill",
        "url":"www.stricklandpropane.com/hank-hill/,
        "email":"hankhill@stricklandpropane.com"
    }
</script>
```