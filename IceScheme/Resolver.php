<?php 
/**
 * This is a reflector class that creates instances of the class to run their methods.
 */
class Resolver {

	/**
	 * Build an instance of the given class
	 *
	 * @param Class $class
	 * @return Reflection Class
	 */
	public function resolve( $class ) {

		$reflector = new \ReflectionClass($class);

		if( ! $reflector->isInstantiable()) {
 			throw new Exception("This class cannot be instantiated.");
 		}
		
 			$constructor = $reflector->getConstructor();
		
 		if(is_null($constructor)) {
 			return new $class;
 		}
		
 		// get params
 		$parameters = $constructor->getParameters();
 		$dependencies = $this->getDependencies($parameters);

 		return $reflector->newInstanceArgs($dependencies);
	}
	
	/**
	 * Build up a list of dependencies for a given methods parameters.
	 *
	 * @param array $parameters
	 * @return array
	 */
	public function getDependencies( $parameters ) {

		// initialize empy $dependencies variable
		$dependencies = [];
		
		foreach($parameters as $parameter) {

			$dependency = $parameter->getClass();

			if(is_null($dependency)) {
				$dependencies[] = $this->resolveNonClass($parameter);
			} else {
				$dependencies[] = $this->resolve($dependency->name);
			}
		}
		
		return $dependencies;
	}
	
	/**
	 * Determine what to do with a non-class value
	 *
	 * @param ReflectionParameter $parameter
	 * @return mixed
	 *
	 * @throws Exception
	 */
	public function resolveNonClass(ReflectionParameter $parameter)
	{
		if($parameter->isDefaultValueAvailable())
		{
			return $parameter->getDefaultValue();
		}
		
		throw new \Exception("Cannot resolve unknown parameter");
	}

}