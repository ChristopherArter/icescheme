<?php
/**
 * 
 * 
 * 				IceScheme - A full-flavored dynamic schema generator for WordPress
 * 
 * 				carter@launchthat.com
 * 
 * 
 * */

class IceSchemeMachine {

	public $json = [];

	function __construct(){

		// Load the resolver class
		include_once('Resolver.php');
		
		// initialize config properties from /config.php
		$this->setConfigParams();
	}

	/**
	 * Sets config settings as object properties
	 *
	 * @return void
	 */
	protected function setConfigParams(){

		$configs = $this->getConfigFile();
		foreach($configs as $key => $value ){
			$this->$key = $value;
		}
		$this->configs = $configs;
	}

	/**
	 * Fetch the value of a specific key in the configs		
	 *
	 * @param string $value
	 * @return string
	 */
	public function getConfigValue( String $value ) {
		
		$configs = $this->getConfigFile();

		if( array_key_exists( $value, $configs ) ){
			return $configs[$value];
		}
	}
	/**
	 * Fetch the contents of configs file as array
	 *
	 * @return void
	 */
	protected function getConfigFile(){

		$json = file_get_contents( dirname(__FILE__) . '/config.json' );
		$decodedJson = json_decode( $json, true );
		return $decodedJson;
	}

	/**
	 * Override array values initally built with builder methods.
	 * @param  Array  $values 		Values to inject into model
	 * @return Object         		Returns instance of itself with new properties.
	 */
	public function with( Array $values ){

		$json = $this->json;
	
		if($json){
			foreach($values as $key => $value){
				if( $value ){
					$json[$key] = $value;
					}
				}
			$this->json = $json;
		}
		return $this;
	}


	
	/**
	 * Changes all "false" strings to null so that the validation tool doesn't see it as a string.
	 * @param array 	$array Array to set to null.
	 */
	public function setNullValues($array){

		foreach($array as $key=>$value){
			if($value == false){
				$array[$key] = null;
			}
		}
		return $array;
	}

	/**
	 * Adds the json to the cache key for this post
	 *
	 * @param string $json
	 * @return void
	 */
	protected function addToSchemaCache($json){

		// set the slug. This is unique based on post id.
		$slug = $this->getUniqueCacheId();

		
		$schemaCache = wp_cache_get($slug);
		
		// if schema cache is not set, set it my dude.
		if(! $schemaCache ){
			wp_cache_set($slug, $json);
			
		// if we DO have this cache set, append the json to it.
		} else if( $schemaCache ){
			$newSchema = $schemaCache . $json;
			wp_cache_replace($slug, $newSchema);
		}
	}
	
	/**
	 * Returns the cache slug with the post id to fetch a unique post ID's cache'd schema slug.
	 *
	 * @return string
	 */
	public function getUniqueCacheId(){
		$uniqueCacheId = $this->cache_slug . get_the_id();
		return $uniqueCacheId;
	}

	/**
	 * Encodes the json property of this class and returns it
	 * @param  String $jsonTags 	Optional tags to pass to json_encode function
	 * @return void           		JSON encoded string of $this->json
	 */
	public function build( $jsonTags = JSON_UNESCAPED_SLASHES ){

		// if json is set, encode and return.
		if( isset($this->json) ){
			$json = json_encode($this->setNullValues($this->json), $jsonTags);
			$this->addToSchemaCache($json);
		} 
	}

	protected function setStringIfIsPost($string){
		if( $string instanceof WP_Post ){
			$postTypes = $this->getConfigValue('post_types');
			$postType = $string->post_type;
			$string = $postTypes[$postType];
		}

		return $string;
	}

	/**
	 * Calls the method associated with the string input. Must have 'Builder' as suffix.
	 * @param  String $string Method to be called.
	 * @param  Object $post   post object to be used to pull data.
	 * @return Object         Returns instance of itself with new properties created from called method.
	 * @throws Exception
	 */
	public function get( $string, $post = null, String $method = null ) {
		if( isset($string) ) {

			$string = $this->setStringIfIsPost($string);

			// include the Flavor class
			$file = $this->getFlavorFromString($string);
	
			// does this file exist?
			if($file){

					// get the flavor passed as string.
					$flavor = new Resolver();
					$flavor = $flavor->resolve($string . $this->flavor_suffix);

				} else {
				
					// there's no flavor with that file name, so remove the json property so that build() will handle this.
					unset($this->json);
					return $this;
				}

			// set the object json based on the given method.
			if(isset($method)) {
				$flavor->json = $flavor->$method($post);
			} else {
				$flavor->json = $flavor->scoop($post);
			}

			// set this object's json to reflect the flavor reflection class's json.
			$this->json = $flavor->json;

		} else {
			throw new Exception("Flavor parameter required.", 1);
		}

		// send this to the build method.
		return $this;
	}

	/**
	 * Does the same thing as get() method but returns array instead of the object. This has a wrapper function called sideOf().
	 *
	 * @param String $string
	 * @param Object $post
	 * @param String $method
	 * @return Array
	 */
	public function flavorAsArray( String $string, $post = null, String $method = null ){

		$flavorAsArray = $this->get($string, $post, $method);
		return $flavorAsArray->json;
	}

	/**
	 * Grab the Flavor associated with this string
	 * @param  String $string Flavor name
	 * @return mixed         Returns the require for given Flavor
	 */
	protected function getFlavorFromString(String $string){

		if(isset($string)){
			if(@include_once( dirname(__FILE__) . $this->flavor_path . $string.'.php')){
				return @include_once( dirname(__FILE__) . $this->flavor_path . $string.'.php');
			}  else {
				return false;
			}
		}
	}

}


