<?php 
    
    /**
     *  
     *  Hospital Flavor Model 
     *
     */

    class HospitalFlavor extends Flavor {



        public function scoop( \WP_Post $post = null ){

            $json = [
                    '@context'  =>  'http://schema.org',
                    '@type'     =>  'Hospital',
                    'name'      =>  $post->post_title,
                    'address'   =>  get_field('address')
            ];

            return $json;
        }


    }

