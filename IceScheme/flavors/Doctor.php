	<?php 
	
	/**
	 *	
	 *	doctors Flavor Model 
	 *
	 */

	class DoctorFlavor extends Flavor {

        public $post_type = 'doctors';


		public function scoop( \WP_Post $post = null ){

            if(! $post ){
                $post = get_post();
            }

			$tc = get_field('treatment_center', $post->ID);
            $hospitalAddress = get_field('address', $tc->ID );
            $hospitalAddress = $hospitalAddress['address'];

            if(get_field('custom_address', $post->ID)){
                $doctorAddress = get_field('address', $post->ID);
                if( isset($doctorAddress['address'])){
                    $doctorAddress = $doctorAddress['address'];
                }
            } else {
                $doctorAddress = $hospitalAddress;
            }

            $jsonDoctor = [
                '@context'              =>  'http://schema.org',
                '@type'                 =>  'Physician',
                'name'                  =>  get_the_title($post),
                'telephone'             =>  get_field('phone', $post->ID),
                'email'                 =>  get_field('email', $post->ID),
                'address'               =>  $doctorAddress,
                'hospitalAffiliation'   =>  sideOf('Hospital', $tc)
            ];

            return $jsonDoctor;
		}

	}