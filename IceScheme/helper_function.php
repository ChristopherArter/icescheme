<?php

	/**
	 * Wraps the IceSchemeMachine->get() method and passes variables dynamically
	 * @param mixed $inputs 	Dynamic inputs passed to get() method
	 */
	function IceScheme( ...$inputs ) {
		return call_user_func_array( array( new IceSchemeMachine(), "get" ), $inputs );
	}

	/**
	 * This is a wrapper for the flavorAsArray() method so that it does not get sent to the build method. This is used exclusively in the flavor model to reference other flavors.
	 *
	 * @param mixed $inputs
	 * @return Array
	 */
	function sideOf( ...$inputs ) {
			return call_user_func_array( array( new IceSchemeMachine(), "flavorAsArray" ), $inputs );
	}

	/**
	 * retrieve a value from configs
	 *
	 * @param mixed $value
	 * @return void
	 */
	function get_config_value( $value ){

		$iceScheme = new IceSchemeMachine();
		return $iceScheme->getConfigValue( $value );
	
	}


	/**
	 * This is the function that calls the cache key associated with the current page and outputs it to the wp_footer hook
	 *
	 * @return Markup
	 */
	function output_icescheme_json(){

			$iceScheme = new IceSchemeMachine();
			$schemaCache = wp_cache_get( $iceScheme->getUniqueCacheId() );
			
			if( $schemaCache ){
				echo '<script type="application/ld+json">' . $schemaCache . '</script>';
			}
	}
	
	// hook the function above into the footer.
	add_action( 'wp_footer', 'output_icescheme_json' );


?>